#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(15, 1067)
        self.assertEqual(v, 179)

    def test_eval_6(self):
        v = collatz_eval(123456, 1234567)
        self.assertEqual(v, 528)

    def test_eval_7(self):
        v = collatz_eval(54979, 243900)
        self.assertEqual(v, 443)

    def test_eval_8(self):
        v = collatz_eval(15625, 123747)
        self.assertEqual(v, 354)

    def test_eval_9(self):
        v = collatz_eval(98236, 1223567)
        self.assertEqual(v, 528)

    def test_eval_10(self):
        v = collatz_eval(5474, 15685)
        self.assertEqual(v, 276)

    def test_eval_11(self):
        v = collatz_eval(138566, 18563)
        self.assertEqual(v, 354)

    def test_eval_12(self):
        v = collatz_eval(54236, 83245)
        self.assertEqual(v, 351)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO(
            "1 10\n100 200\n201 210\n900 1000\n15 1067\n123456 1234567\n 54979 243900\n15625 123747\n 98236 1223567\n5474 15685\n138566 18563\n54236 83245\n"
        )
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n15 1067 179\n123456 1234567 528\n54979 243900 443\n15625 123747 354\n98236 1223567 528\n5474 15685 276\n138566 18563 354\n54236 83245 351\n",
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
